package np.com.naxa.sankhu.sudur.Interface;

/**
 * Created by nishon.tan on 11/7/2016.
 */

public interface OnPlacesTaskCompleted {
    void onPlacesTaskCompleted(String response);
}
