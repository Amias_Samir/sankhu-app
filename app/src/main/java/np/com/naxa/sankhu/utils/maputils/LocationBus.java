package np.com.naxa.sankhu.utils.maputils;

import android.location.Location;

public interface LocationBus {

    public void locationTransporter(Location location);
}
