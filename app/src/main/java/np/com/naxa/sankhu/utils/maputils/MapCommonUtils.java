package np.com.naxa.sankhu.utils.maputils;

import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

public class MapCommonUtils {

    public static void zoomToMapBoundary(final MapView mapView,final GeoPoint centerPoint){
//        BoundingBox boundingBox = new BoundingBox(27.737334, 85.534334, 27.647866, 85.386996);

        BoundingBox boundingBox = new BoundingBox(27.8621067722564, 85.615843891757, 27.6557573184055,  85.3581820853735);

        mapView.zoomToBoundingBox(boundingBox, true);
        mapView.setScrollableAreaLimitDouble(boundingBox);
        mapView.getController().animateTo(centerPoint);
        mapView.invalidate();
    }
}
